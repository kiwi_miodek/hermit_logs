# Installation

Run commands:

    composer require hermit/logs
    php artisan logs:install

# Usage
In order to write a log simply call to a function described in [PSR-3 LoggerInterface](https://www.php-fig.org/psr/psr-3/) from `Hermit\Logs\Logger::class`.

Example:
    
    \Hermit\Logs\Logger::error('message');
    
    \Hermit\Logs\Logger::error('warning',['author_id'=>5]);
    
    \Hermit\Logs\Logger::log('type','message',['priority'=>5, 'comment'=>'Log comment']);

`type` - string; type of log, helps categorizing logs - those categories are then used as category 
`message` - text; main description; it is possible to pass objects as message parameter (as log as it is possible to use json_encode() function on them)
## Context array description

`priority` - integer - default `1`; priority of log;

`comment` - text - default `null`; additional description; sometimes it maybe more useful to put some informations here

`author` or `author_id` - App\User object or integer identifying author of log;

Any other entries will be seen only on logs saved to file.

## Configuration
You can configure logs options in `config\logger.php` file or by adding to your projects .env file:
```
LOGS_ENABLED=(true|false)
LOG_DESTINATION=(db|file)
LOG_FILE_PATH=logs
LOG_SAVE_METHOD=(direct|redis)
LOGS_REDIS_BATCH=100
LOG_MAX_FILES=14
LOGS_AUTOMATIC_ARCHIVE=(true|false)
LOGS_LIFESPAN=3
LOGS_ARCHIVE_BATCH=1000
```

## Routes
    Route::prefix('logs')->name('logs.')->group(function () {
        Route::get('/', 'hermit\logs\MainLogController@index')->name('index');
        Route::post('/priorities', 'hermit\logs\MainLogController@savePriorities')->name('savePriorities');
        Route::prefix('archives')->name('archives.')->group(function () {
            Route::get('/', 'hermit\logs\MainLogArchiveController@index')->name('index');
            Route::get('/details/{logArchive}', 'hermit\logs\MainLogArchiveController@details')->name('details');
        });
    });

In order to add authentication on log routes you can add those routes to your project web.php routes file and secure it in on your own.

## Publishable files
You can publish 3 files from this package and edit them as you wish.

    php artisan vendor:publish
    
* `hermit-logs-controller` - LogController.php - extends MainLogController.php; you can override all methods defined in MainLogController class as you wish
* `hermit-logs-archives-controller` -  LogArchiveController.php - extends MainLogArchiveController.php; you can override all methods defined in MainLogController class as you wish
* `hermit-logs-views` - prioritiesForm.blade.php - simple form for setting priorities for existing logs categories


## Priorities
When log is being saved it's priority is compared with minimum value of `MAIN_PRIORITY` and defined `TYPE` priority. If log priority is lower then max of those two then log is written, otherwise nothing happens.

## Archiving Logs
In time logs will take more and more space in your database.

You can archive logs in order to save some space using command:

    php artisan logs:archive

# Updates
## * => 2.0
* Run command `php artisan logs:update`
