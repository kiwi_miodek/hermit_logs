<?php

Route::prefix('logs')->name('logs.')->middleware('web')->group(function () {
    Route::get('/', 'hermit\logs\MainLogController@index')->name('index');
    Route::post('/priorities', 'hermit\logs\MainLogController@savePriorities')->name('savePriorities');
    Route::prefix('archives')->name('archives.')->group(function () {
        Route::get('/', 'hermit\logs\MainLogArchiveController@index')->name('index');
        Route::get('/details/{logArchive}', 'hermit\logs\MainLogArchiveController@details')->name('details');
    });
});
