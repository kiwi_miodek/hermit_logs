<?php

namespace Hermit\Logs;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redis;

class Logger
{
    const LOG_LEVEL = [
        'EMERGENCY ' => 'emergency',
        'ALERT' => 'alert',
        'CRITICAL' => 'critical',
        'ERROR' => 'error',
        'WARNING' => 'warning',
        'NOTICE' => 'notice',
        'INFO' => 'info',
        'DEBUG' => 'debug',
    ];

    private const METHODS = [
        'direct',
        'redis'
    ];

    private const DESTINATIONS = [
        'db',
        'file'
    ];

    private const DIRECT_LEVELS = [
        'emergency',
        'alert',
        'critical',
        'error',
        'log_error'
    ];

    /**
     * System is unusable.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    static public function emergency($message, array $context = array())
    {
        Logger::log(Logger::LOG_LEVEL['EMERGENCY'], $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    static public function alert($message, array $context = array())
    {
        Logger::log(Logger::LOG_LEVEL['LogLevel::ALERT'], $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    static public function critical($message, array $context = array())
    {
        Logger::log(Logger::LOG_LEVEL['CRITICAL'], $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    static public function error($message, array $context = array())
    {
        Logger::log(Logger::LOG_LEVEL['ERROR'], $message, $context);
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    static public function warning($message, array $context = array())
    {
        Logger::log(Logger::LOG_LEVEL['WARNING'], $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    static public function notice($message, array $context = array())
    {
        Logger::log(Logger::LOG_LEVEL['NOTICE'], $message, $context);
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    static public function info($message, array $context = array())
    {
        Logger::log(Logger::LOG_LEVEL['INFO'], $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string $message
     * @param array $context
     *
     * @return void
     */
    static public function debug($message, array $context = array())
    {
        Logger::log(Logger::LOG_LEVEL['DEBUG'], $message, $context);
    }

    /**
     * Logs with custom level (type).
     *
     * @param string $level
     * @param string $message
     * @param array $context
     * @return bool
     */
    static public function log($level, $message, array $context = array())
    {
        if (!config('logger.enabled')) {
            return false;
        }
        $data = self::prepareData($level, $message, $context);
        if (self::allowSave($data)) {
            if (in_array($level, self::DIRECT_LEVELS)) {
                self::direct($data);
            } else {
                $method = config('logger.save_method');
                if (in_array($method, self::METHODS)) {
                    self::$method($data);
                } else {
                    $error_message = 'Log method "' . $method . '" not found.';
                    self::logError($error_message, $data);
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Check log priority with stored priorities.
     * If log priority is lower than set priority for given type return true.
     *
     * @param array $data
     * @return bool
     */
    static private function allowSave($data)
    {
        $type = $data['type'];
        $priority = $data['priority'];
        try {
            $types = self::getLogTypes();
        } catch (\Exception $exception) {
            $data['exception'] = $exception;
            self::logError($exception->getMessage(), $data);
            return false;
        }
        $mainPriority = $types->where('name', 'MAIN_PRIORITY')->pluck('priority')->first();
        if (!in_array(str_replace(' ', '_', strtoupper($type)), $types->pluck('name')->toArray())) {
            $logType = new LogType([
                'name' => str_replace(' ', '_', strtoupper($type)),
                'priority' => 10
            ]);
            $logType->save();
            Cache::delete("HermitLogTypes");
            self::getLogTypes();
        } else {
            $logType = $types->where('name', str_replace(' ', '_', strtoupper($type)))->first();
        }
        $typePriority = $logType->priority;
        $output = false;
        if (min($mainPriority, $typePriority) >= $priority) {
            $output = true;
        }
        return $output;
    }

    static private function getLogTypes()
    {
        $types = Cache::get("HermitLogTypes");
        if (!$types) {
            $types = LogType::all();
            $expiresAt = now()->addDays(7);
            Cache::put("HermitLogTypes", $types, $expiresAt);
        }
        return $types;
    }

    /**
     * Logging error logs. IE when there is an error with database connection and logs are set to be saved in database.
     *
     * @param string $error_message
     * @param array $data
     */
    static public function logError($error_message, array $data)
    {
        self::file([
            'type' => 'LOG_ERROR',
            'message' => $error_message,
            'data' => $data
        ]);
        if (key_exists('exception', $data) && $data['type'] != 'LOG_ERROR') {
            throw $data['exception'];
        }
    }

    /**
     * Function is public because it is used by logs:redis command.
     *
     * @param $data
     * @return bool
     */
    static public function save($data)
    {
        if (!self::validateData($data)) {
            self::logError('invalid data given to Logger::save() method.', $data);
            return false;
        }
        if (!key_exists('destination', $data)) {
            $data = self::prepareData($data['type'], $data['message'], $data);
        }
        if (!key_exists('author_id', $data)) {
            $data['author_id'] = auth()->id();
        }
        if (!key_exists('comment', $data)) {
            $data['comment'] = '';
        }
        if (!key_exists('ip', $data)) {
            $data['ip'] = '';
            if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
                $data['ip'] = $_SERVER['REMOTE_ADDR'];
            }
        }
        $destination = $data['destination'];
        self::$destination($data);
        return true;
    }


    static private function prepareData($level, $message, array $context = array())
    {
        $comment = null;
        $priority = 1;
        $author_id = auth()->id();
        if (key_exists('comment', $context)) {
            $comment = $context['comment'];
        }
        if (key_exists('priority', $context)) {
            $priority = $context['priority'];
        }
        if (key_exists('author', $context)) {
            $author_id = $context['author'];
            if ($author_id instanceof User) {
                $author_id = $author_id->id;
            }
        }
        if (key_exists('author_id', $context)) {
            $author_id = $context['author_id'];
        }
        $message = self::interpolate($message, $context);
        $ip = null;
        if (array_key_exists('REMOTE_ADDR', $_SERVER)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if (is_string($message) || is_numeric($message)) {
            $message = str_limit(strip_tags($message, "<br><b><i><u>"), 60000);
        } else {
            $message = json_encode($message);
        }
        $message = str_limit($message, 60000);
        $data = [
            'type' => str_replace(' ', '_', strtoupper($level)),
            'message' => $message,
            'priority' => $priority,
            'comment' => str_limit(strip_tags($comment, "<br><b><i><u>"), 60000),
            'ip' => $ip,
            'author_id' => $author_id,
            'context' => $context,
            'date' => now()->format('Y-m-d H:i:s'),
            'destination' => config('logger.destination')
        ];
        return $data;
    }

    /**
     * Interpolates context values into the message placeholders.
     */
    static private function interpolate($message, array $context = array())
    {
        // build a replacement array with braces around the context keys
        $replace = array();
        foreach ($context as $key => $val) {
            // check that the value can be casted to string
            if (!is_array($val) && (!is_object($val) || method_exists($val, '__toString'))) {
                $replace['{' . $key . '}'] = $val;
            }
        }

        // interpolate replacement values into the message and return
        return strtr($message, $replace);
    }


    static private function direct($data)
    {
        if (in_array($data['destination'], self::DESTINATIONS)) {
            self::save($data);
        } else {
            $error_message = 'Wrong logs destination: "' . $data['destination'] . '".';
            self::logError($error_message, $data);
        }
    }

    static private function redis($data)
    {
        Redis::rpush('logger', json_encode($data));
    }

    static private function validateData($data)
    {
        $required_keys = [
            'type',
            'message',
            'date'
        ];
        foreach ($required_keys as $required_key) {
            if (!key_exists($required_key, $data)) {
                return false;
            }
        }
        return true;
    }

    static private function db($data)
    {
        try {
            Log::create([
                'type' => $data['type'],
                'message' => $data['message'],
                'priority' => $data['priority'],
                'comments' => $data['comment'],
                'author_id' => $data['author_id'],
                'ip' => $data['ip'],
                'date' => Carbon::parse($data['date'])->format('Y-m-d H:i:s'),
            ]);
        } catch (\Exception $exception) {
            $data['exception'] = $exception;
            self::logError($exception->getMessage(), $data);
        }
    }

    static private function file($data)
    {
        $file_path = config('logger.file_path');
        if (!File::exists(storage_path($file_path))) {
            File::makeDirectory(storage_path($file_path), 0755, true, true);
            if (!File::exists(storage_path($file_path) . '/.gitignore')) {
                File::put(storage_path($file_path) . '/.gitignore', '*
!.gitignore
');
            }
        }
        if ($data['type'] == 'LOG_ERROR') {
            $exception_string = '';
            $stack_trace = '';
            if (key_exists('exception', $data['data'])) {
                $exception = $data['data']['exception'];
                unset($data['data']['exception']);
                $exception_string = ' message: ' . $exception->getMessage() . ' file: ' . $exception->getFile() . ':' . $exception->getLine();
                $stack_trace = PHP_EOL . 'stack_trace: ' . PHP_EOL . $exception->getTraceAsString();
            }
            $string = '[' . now()->format('Y-m-d H:i:s') . '] ' . $data['type']
                . $exception_string
                . PHP_EOL . ' - original_log_data: ' . json_encode($data['data'])
                . $stack_trace . PHP_EOL;
        } else {
            $string = '[' . $data['date'] . '] ' . $data['type'] . '(' . $data['priority'] . ')' . ' ' . $data['message'];
            if ($data['comment']) {
                $string .= ' | comment: ' . $data['comment'];
            }
            if ($data['author_id']) {
                $string .= ' | author_id:' . $data['author_id'];
            }
            if ($data['ip']) {
                $string .= '; ip:' . $data['ip'];
            }
            if (!empty($data['context'])) {
                $string .= '; context: ' . json_encode($data['context']);
            }
        }
        $file = '';
        $file_name = 'logger-' . now()->format('Y-m-d') . '.log';
        if (File::exists(storage_path($file_path) . '/' . $file_name)) {
            $file = File::get(storage_path($file_path) . '/' . $file_name);
        }
        $file .= $string . PHP_EOL;
        File::put(storage_path($file_path) . '/' . $file_name, $file);
    }
}
