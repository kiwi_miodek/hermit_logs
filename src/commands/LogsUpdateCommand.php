<?php

namespace Hermit\Logs;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class LogsUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Artisan::queue('migrate', ['-q' => '']);
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "hermit-logs-config", '--force' => 'true']);
        Artisan::queue('config:cache');
        echo 'UPDATED';
    }
}
