<?php

namespace Hermit\Logs;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class LogsInstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->confirm('In order to install logs correctly insert " //forest_here " inside report() method in your projects Exceptions\Handler.php file. Are you done?')) {
            $this->install();
        }
        echo 'DONE';
    }

    protected function install()
    {
        Artisan::queue('migrate', ['-q' => '']);
        echo 'Migration done' . PHP_EOL;
        Artisan::queue('vendor:publish', ['-q' => '', "--tag" => "hermit-logs-config", '--force' => 'true']);
        if (File::exists(app_path() . '/Exceptions/Handler.php')) {
            $errorHandler = File::get(app_path() . '/Exceptions/Handler.php');
            $errorHandler = str_replace('use Exception;', 'use Exception;
use Hermit\Logs\Log;
use Hermit\Logs\Logger;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;', $errorHandler);
            $errorHandler = str_replace('protected $dontReport = [', 'protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Validation\ValidationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,', $errorHandler);
            $errorHandler = str_replace('//forest_here', '  if (Schema::hasTable(\'logs\') && Schema::hasTable(\'log_types\')) {
            if ($this->shouldReport($exception)) {
                $message = strip_tags($exception->getMessage());
                try {
                    Logger::error($message, [
                        \'comment\' => \' File: \' . $exception->getFile() . \' : \' . $exception->getLine()
                    ]);
                } catch (Exception $e) {
                    Logger::log(\'log_error\', $exception->getMessage());
                }
            }
        }', $errorHandler);
            File::put(app_path() . '/Exceptions/Handler.php', $errorHandler);
            echo 'Error Handler modified' . PHP_EOL;
        }
        Artisan::queue('config:cache');
        Log::write('ERROR', 'log for testing ' . Log::all()->count());
    }
}
