<?php

namespace Hermit\Logs;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ArchiveLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:archive {--automatic} {--files}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ($this->option('automatic')) {
            $this->automatic();
        } elseif ($this->option('files')) {
            $this->files();
        } else {
            $this->manualArchive();
        }

    }

    private function files()
    {
        $path = config('logger.file_path');
        $files = File::allFiles(storage_path($path));
        $logger_files = [];
        foreach ($files as $key => $file) {
            if (substr($file->getFilename(), 0, 6) == 'logger') {
                $logger_files[substr($file->getFilename(), 9, 10)] = $file;
            }
        }
        $max_nr_of_files = config('logger.max_number_of_files');
        if (count($logger_files) > $max_nr_of_files) {
            foreach ($logger_files as $key => $file) {
                unset($logger_files[$key]);
                File::delete(storage_path($path) . '/' . $file->getFileName());
                if (count($logger_files) == $max_nr_of_files) {
                    break;
                }
            }
        }
    }

    private function automatic()
    {
        $batch = config('logger.archive_batch', 1000);
        $months = config('logger.db_lifespan');
        $where = [];
        $logs = Log::where('created_at', '<', now()->subMonths($months))->count();
        $iterations = (int)($logs / $batch);
        $rest = $logs % $batch;
        $log_type = 'Older than ' . $months . ' Months';
        $this->processLogs($where, $log_type, $iterations, $rest, $batch);
    }

    private function processLogs($where, $log_type, $iterations, $rest, $batch)
    {
        $i = 0;
        DB::beginTransaction();
        try {
            echo "Progress: 0% \r";

            while ($i < $iterations) {
                $this->archive($batch, $where, $log_type);
                echo "Progress: " . round($i / $iterations * 100, 2) . "% \r";
                $i++;
            }
            if ($rest) {
                $this->archive($rest, $where, $log_type);
            }
            echo "Progress: 100%   " . PHP_EOL;
            DB::commit();
        } catch (\Exception $exception) {
            DB::rollBack();
            echo "There was an error. Rolled back changes. " . PHP_EOL . "ERROR: " . $exception->getMessage();
        }
    }

    private function archive($batch, $where, $log_type)
    {
        $logs_builder = Log::where($where)->take($batch);
        $logs_data = $logs_builder->get();
        if (count($logs_data) == 0) {
            return 0;
        }
        $content = gzcompress($logs_data->toJson(), 1);
        LogArchive::create([
            'content' => $content,
            'from' => $logs_data->min('date'),
            'to' => $logs_data->max('date'),
            'comment' => $log_type
        ]);
        $logs_builder->delete();
    }

    private function manualArchive()
    {
        $batch = config('logger.archive_batch', 1000);
        $where = [];
        $iterations = 0;
        $rest = 0;

        $time = $this->confirm('Would you like to archive logs older than given number of months?');

        if ($time) {
            $months = $this->ask('Logs older than given amount of moths will be archives. Enter amount: ');
            $logs = Log::where('date', '<', now()->subMonths($months))->count();
            $iterations = (int)($logs / $batch);
            $rest = $logs % $batch;
            $log_type = 'Older than ' . $months . ' Months';
        } else {
            $log_type = $this->ask('Enter log type to archive (empty = all): ');
            $number = $this->ask('Enter amount of logs to archive (empty = all): ');
            if (!$log_type && !$number) {
                $confirm = $this->confirm('Do you want to archive all logs?');
                if (!$confirm) {
                    return 0;
                }
            }
            if ($log_type) {
                $where[] = ['type', $log_type];
            }
            if ($number) {
                if ($number > $batch) {
                    $iterations = (int)($number / $batch);
                    $rest = $number % $batch;
                } else {
                    $rest = $number;
                }
            } else {
                $logs = Log::where($where)->count();
                $iterations = (int)($logs / $batch);
                $rest = $logs % $batch;
            }
        }
        echo 'i: ' . $iterations . ' r: ' . $rest . PHP_EOL;
        $this->processLogs($where, $log_type, $iterations, $rest, $batch);
    }
}
