<?php

namespace Hermit\Logs;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class RedisHandleLogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'logs:redis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!Redis::llen('logger')) {
            return 0;
        }
        $batch_amount = config('logger.redis_save_batch');
        if (!$batch_amount) {
            $batch_amount = 100;
        }
        $list_length = Redis::llen('logger');
        while (($list_length > 0) && ($batch_amount > 0)) {
            echo $list_length . PHP_EOL;
            $data = Redis::lpop('logger');
            Logger::save(json_decode($data, 1));
            $list_length = Redis::llen('logger');
            $batch_amount--;
        }
    }
}
