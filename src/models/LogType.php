<?php

namespace Hermit\Logs;

use Illuminate\Database\Eloquent\Model;

class LogType extends Model
{
    protected $fillable = [
        'name', 'priority',
    ];

}
