<?php

namespace Hermit\Logs;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'type', 'message', 'priority',
        'comments', 'ip', 'author_id',
        'date'
    ];

    static public function write($type, $message, $priority = 1, $comment = null, $author_id = null)
    {
        if (!$author_id) {
            $author_id = auth()->id();
        }
        $context = [
            'priority' => $priority,
            'comment' => $comment,
            'author_id' => $author_id
        ];
        Logger::log($type, $message, $context);
    }
}
