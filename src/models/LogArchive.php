<?php

namespace Hermit\Logs;

use Illuminate\Database\Eloquent\Model;

class LogArchive extends Model
{

    protected $table = 'log_archives';

    protected $fillable = [
        'content',
        'from',
        'to',
        'comment'
    ];
}
