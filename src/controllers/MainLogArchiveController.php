<?php

namespace Hermit\Logs;

use App\Http\Controllers\Controller;

class MainLogArchiveController extends Controller
{
    public function index()
    {
        $logArchives = LogArchive::all();
        return view('logs::archives.index', compact('logArchives'));
    }

    public function details(LogArchive $logArchive)
    {
        $data = json_decode(gzuncompress($logArchive->content));
        $data = collect($data);
        $data->sortByDesc('id');
        return view('logs::archives.details', compact('logArchive', 'data'));
    }
}
