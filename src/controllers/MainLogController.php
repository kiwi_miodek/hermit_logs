<?php

namespace Hermit\Logs;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainLogController extends Controller
{
    public function index()
    {
        $types = MainLogController::getLogTypes();
        $allLogs = Log::orderBy('date', 'desc')->get();
        $logs = [];
        foreach ($types as $type) {
            $logs[$type->name] = $allLogs->where('type', $type->name);
        }
        return view('logs::index', ['types' => $types, 'logs' => $logs]);
    }

    /**
     * Returns collection of LogType objects without MAIN_PRIORITY
     * @return mixed
     */
    public function getLogTypes()
    {
        return LogType::where('name', '!=', 'MAIN_PRIORITY')->get();
    }

    public function savePriorities(Request $request)
    {
        $new = $request->all();
        $logTypes = LogType::all();
        foreach ($logTypes as $logType) {
            if (key_exists($logType->name, $new)) {
                $logType->priority = $new[$logType->name];
                $logType->save();
            }
        }
        return redirect()->route('logs.index');
    }
}
