<!DOCTYPE html>
<html>
<head>
    <title>Hermit Logs</title>
    <style>
        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons that are used to open the tab content */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }

        .log-priority-form {
            width: 255px;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        @include('logs::prioritiesForm')
        <hr>
        <a href="{{route('logs.archives.index')}}">Log Archives</a>
        <hr>
        <!-- Tab links -->
        <div class="tab">
            @foreach($types as $type)
                <button class="tablinks" onclick="openLogs(event, '{{$type->name}}')">{{$type->name}}</button>
            @endforeach
        </div>
        <!-- Tab content -->
        @foreach($types as $type)
            <div id="{{$type->name}}" class="tabcontent">
                <table border="1px" style="width: 100%;">
                    <tr>
                        <th style="width: 3%">ID</th>
                        <th style="width: 5%">TYPE</th>
                        <th style="width: 50%">DESCRIPTION</th>
                        <th style="width: 10%">AUTHOR</th>
                        <th style="width: 15%">COMMENT</th>
                        <th style="width: 8%">IP</th>
                        <th style="width: 8%">DATE</th>
                    </tr>
                    @foreach($logs[$type->name] as $log)
                        <tr>
                            <td>{{$log->id}}</td>
                            <td>{{$log->type}}</td>
                            <td>{{$log->message}}</td>
                            <td>{{$log->author_id}}</td>
                            <td>{{$log->comments}}</td>
                            <td>{{$log->ip}}</td>
                            <td>{{$log->date}}</td>
                        </tr>
                    @endforeach
                </table>
            </div>
        @endforeach
    </div>
</div>
</body>
<script>
    function openLogs(evt, cityName) {
        // Declare all variables
        var i, tabcontent, tablinks;

        // Get all elements with class="tabcontent" and hide them
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        // Get all elements with class="tablinks" and remove the class "active"
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }

        // Show the current tab, and add an "active" class to the button that opened the tab
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    var tab = document.getElementsByClassName('tab')[0];
    var child = tab.childNodes[1];
    child.click();
</script>
</html>
