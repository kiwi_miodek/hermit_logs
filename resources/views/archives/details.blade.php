<!DOCTYPE html>
<html>
<head>
    <title>Hermit Logs</title>
    <style>
        /* Style the tab */
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        /* Style the buttons that are used to open the tab content */
        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current tablink class */
        .tab button.active {
            background-color: #ccc;
        }

        /* Style the tab content */
        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }

        .log-priority-form {
            width: 255px;
        }
    </style>
</head>
<body>
<div class="container">
    <a href="{{route('logs.index')}}">BACK</a>
    <hr>
    <div class="content">
        <table border="1px" style="width: 100%;">
            <tr>
                <th style="width: 3%">ID</th>
                <th style="width: 5%">TYPE</th>
                <th style="width: 50%">DESCRIPTION</th>
                <th style="width: 10%">AUTHOR</th>
                <th style="width: 15%">COMMENT</th>
                <th style="width: 8%">IP</th>
                <th style="width: 8%">DATE</th>
            </tr>
            @foreach($data as $log)
                <tr>
                    <td>{{$log->id}}</td>
                    <td>{{$log->type}}</td>
                    <td>{{$log->message}}</td>
                    <td>{{$log->author_id}}</td>
                    <td>{{$log->comments}}</td>
                    <td>{{$log->ip}}</td>
                    <td>{{$log->created_at}}</td>
                </tr>
            @endforeach
        </table>
    </div>
</div>
</body>
</html>
