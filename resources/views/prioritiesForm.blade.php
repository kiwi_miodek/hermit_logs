<form class="log-priority-form" action="{{route('logs.savePriorities')}}" method="POST" id="log-priority-form">
    <div class="log-priority-form-inner-container">
        @foreach(\Hermit\Logs\LogType::all() as $logType)
            <div class="log-priority-field">
                <label for="{{$logType->name}}" class="log-priority-label">{{$logType->name}}</label>
                <div class="log-priority-input-container">
                    <input class="log-priority-input" id="log-priority-{{$logType->name}}" name="{{$logType->name}}"
                           value="{{$logType->priority}}" type="number" min="1">
                </div>
            </div>
        @endforeach
        <hr>
        <div class="log-priority-form-actions">
            <div class="log-priority-button-container">
                <button type="submit" class="log-priority-button" id="log-priority-button-save">
                    Save
                </button>
            </div>
        </div>
        @csrf
        @method("POST")
    </div>
</form>
