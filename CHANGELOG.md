#2.0
* adjusted methods with [PSR-3 LoggerInterface](https://www.php-fig.org/psr/psr-3/)
* added option to save logs to file instead of database
* added save method by redis - at the moment of calling method data is stored in Redis list and then stored to defined destination by `php artisan logs:redis` command
* updated README.md
#2.1
* moved priority checking from save method to log method - this change will prevent inserting log data into redis list when using redis as save method (previously data was inserted into redis list and priority was checked after that causing unnecessary actions and redis memory consumption)
* changed taking priority to check from max of MainPriority and TypePriority to min of those two
#2.2
* added storing log types in cache in order to prevent multiple database queries
#2.3
* added handling author id on Log::write()
